# FileSweeper


FileSweeper is a very basic python program build to clean folders based on file criterias like modification_date and size.
Registered as a cron or systemd tasks, it will clean periodically folders of logs files, cameras snaphots or records, or every other repeteadly created files that takes disk space.

As of now, this is the actual criteria list :

	- date_min (-dmin) : Minimum modification date for a file to be kept. Older files will be deleted. Date must be in ISO format (ex. 2020-19-02)
	- date_max (-dmax) : Maximum modification date for a file to be kept. More recent files will be deleted. Date must be in ISO format (ex. 2020-19-02)
	- newer_than (-n) : File newer than number of days given will be kept. Files older than number of days given will be removed.
	- max_size (s) : Maximum size the file must be to be kept. Size is in Mo (Megabytes).
	
## How to use it

1. Download it
2. Allow it to be executed if it is not the case (chmod +x ./filesweeper.py in the directory where the program is) 
3. Either run it once (see use cases below) or register it as as service or a cron instructions
4. Enjoy

### Exemples

Delete files older than 10 days : 
`./filesweeper.py --folder your_folder_here -newer_than 10` where your_folder_here is the folder to clean

Delete files older than 2020-02-01 : 
`./filesweeper.py --folder your_folder_here -min_date 2020-02-01` where your_folder_here is the folder to clean

Delete files newer than 2020-02-01 : 
`./filesweeper.py --folder your_folder_here -max_date 2020-02-01` where your_folder_here is the folder to clean

Delete files bigger than 5Mo : 
`./filesweeper.py --folder your_folder_here -max_size 5` where your_folder_here is the folder to clean

### Run it in verbose mode 
You can run it in verbose mode with -v parameter.