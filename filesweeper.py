#!/usr/bin/env python3

# File archiving script - Manage files and keep folders clean
# Version : 0.1
# Maintainer:  nicolas@entreprises-connectees.fr

# Include standard modules
import argparse

import shutil
import os
import time
import sys

from datetime import *

# Constants

# Define the program description
description = 'File sweeper is build to delete files no longer needed in a folder based on multiple criterias: too old, too big, never accessed etc...'
files_deleted_count = 0
newer_than_in_seconds = None
date_min = None
date_max = None
max_size_in_bytes = None


# Initiate the parser with a description
parser = argparse.ArgumentParser(description = description)

parser.add_argument("-v", "--verbose", help = "Activate verbose mode", action = "store_true")
parser.add_argument("--folder", "-f", help = "Set the target folder to sweep.", required = True)
parser.add_argument("--newer_than", "-n", help = "File newer than number of days will be kept.")
parser.add_argument("--min_date", "-mid", type = date.fromisoformat, help = "Set the minimum date (at 0am) when the files have to be kept. Must be in ISO format.")
parser.add_argument("--max_date", "-M", type = date.fromisoformat, help = "Set the maximum date (at 0am) when the files have to be kept. Must be in ISO format.")
parser.add_argument("--max_size", "-s", type = float, help = "Set the maximum size the file must be to be kept. Size is in Mo (Megabytes).")
parser.add_argument("-e", "--empty", help = "Delete empty files.", action = "store_true")

args = parser.parse_args()

if args.verbose:
    print('-----------------------------------------------------------')
    print('- File sweeper - Here we go !                                ')                                                
    print('-----------------------------------------------------------')
    
if args.verbose:
    print("> Executing filesweeper in verbose mode.")

if args.folder:
    if args.verbose:
        print("+ Folder chosen : " + args.folder )
    target_folder = args.folder

if args.newer_than:
    if args.verbose:
        print("+ Criteria : Must be newer than " + args.newer_than + " days to be kept." )
    newer_than_in_seconds = int(args.newer_than) * 60 * 60 * 24

if args.min_date:
    if args.verbose:
        print("+ Criteria : Must be newer than " + str(args.min_date) + " to be kept." )
    date_min = args.min_date

if args.max_date:
    if args.verbose:
        print("+ Criteria : Must be older than " + str(args.max_date) + " to be kept." )
    date_max = args.max_date

if args.max_size:
    if args.verbose:
        print("+ Criteria : Must be lighter than " + str(args.max_size) + "Mo to be kept." )
    max_size_in_bytes = args.max_size * 1000000

if args.empty:
    if args.verbose:
        print("+ Criteria : Must not be empty." )

files_to_look_at = os.listdir(target_folder)

for file_to_look_at in files_to_look_at:

    if (not newer_than_in_seconds is None):

        if (os.path.getmtime(target_folder + file_to_look_at) < (time.time() - newer_than_in_seconds)):
            
            if args.verbose:
                sys.stdout.write('- Deleting "' + file_to_look_at + '", it is now too old...')
            
            os.remove(target_folder + file_to_look_at)
            files_deleted_count = files_deleted_count + 1
            
            if args.verbose:
                sys.stdout.write('deleted.\n')

    if (not date_min is None):

        if (date.fromisoformat(date.isoformat(datetime.fromtimestamp(os.path.getmtime(target_folder + file_to_look_at)))) < date_min):
            
            if args.verbose:
                sys.stdout.write('- Deleting "' + file_to_look_at + '", it is now too old...')
            
            os.remove(target_folder + file_to_look_at)
            files_deleted_count = files_deleted_count + 1
            
            if args.verbose:
                sys.stdout.write('deleted.\n')

    if (not date_max is None):

        if (date.fromisoformat(date.isoformat(datetime.fromtimestamp(os.path.getmtime(target_folder + file_to_look_at)))) > date_max):
            
            if args.verbose:
                sys.stdout.write('- Deleting "' + file_to_look_at + '", it is too new...')
            
            os.remove(target_folder + file_to_look_at)
            files_deleted_count = files_deleted_count + 1

            if args.verbose:
                sys.stdout.write('deleted.\n')

    if (not max_size_in_bytes is None):

        if (os.stat(target_folder + file_to_look_at).st_size >= max_size_in_bytes):

            if args.verbose:
                sys.stdout.write('- Deleting "' + file_to_look_at + '", it is too big...')
            
            os.remove(target_folder + file_to_look_at)
            files_deleted_count = files_deleted_count + 1
            
            if args.verbose:
                sys.stdout.write('deleted.\n')

    if args.empty:

        if (os.stat(target_folder + file_to_look_at).st_size == 0):

            if args.verbose:
                sys.stdout.write('- Deleting "' + file_to_look_at + '", it is empty...')
            
            os.remove(target_folder + file_to_look_at)
            files_deleted_count = files_deleted_count + 1

            if args.verbose:
                sys.stdout.write('deleted.\n')

if args.verbose:
    print('> I\'m done.')

print('> Deleted ' + str(files_deleted_count) + ' files.')

exit(0)